//
//  AlertFunctions.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 10.12.2017
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import Foundation
import UIKit
import LocalAuthentication

var error: NSError?
var policyError: NSError?

func showAlertWithOneButton(_ title: String, message: String, withAction: String) {
    let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
    UIApplication.shared.keyWindow?.rootViewController?.presentedViewController?.present(alertView, animated: true, completion: nil)
    let action = UIAlertAction(title: withAction, style: .default, handler: nil)
    alertView.addAction(action)
}

func useTouchID(handler: @escaping (_ error: String?) -> Void) {
    let authC = LAContext()
    
    guard authC.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) else {
        showAlertWithOneButton("Cant Enter", message: "Couldn't find any Touch ID in the Device", withAction: "OK")
        handler(error?.localizedDescription)
        return
    }
    
    authC.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: "To protect your device settings", reply: { (success, error) -> Void in
        if(success) {
            // Fingerprint recognized
            // Go to view controller
            handler(nil)
        } else {
            // Check if there is an error
            if let error = error {
                let errorNS = error as NSError
                switch errorNS.code {
                case LAError.userFallback.rawValue:
                    handler(nil)
                default:
                    handler(error.localizedDescription)
                }
            }
        }
    })
}

