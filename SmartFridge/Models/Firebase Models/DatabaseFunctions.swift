//
//  DatabaseFunctions.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 17.12.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import Foundation
import FirebaseDatabase

func saveToEntity(object: ShelfObject) {
    NSLog("Saving \(String(describing: object.type)) to Entity Database")
    saveShelf(shelf: object)
}

func getShelvesFromFirebase() {
    let ref = Database.database().reference()
    ref.child("Shelves").observe(.value) { (snapshot) in
        let snapshotDict = snapshot.value as? NSDictionary
        if snapshotDict != nil {
            for item in snapshotDict! {
                let itemDict = item.value as? NSDictionary
                if itemDict != nil {
                    let curWeight = itemDict?["currentWeight"] as? Double ?? 0.0
                    let labelColor = itemDict?["labelColor"] as? Int ?? 0
                    let type = itemDict?["type"] as? String ?? ""
                    let limitWeight = itemDict?["limitWeight"] as? Double ?? 0.0
                    let id = itemDict?["id"] as? Int ?? 0
                    let object = ShelfObject(ID: id, type: type, labelColor: labelColor, limitWeight: limitWeight as NSNumber, current: curWeight as NSNumber)
                    saveToEntity(object: object)
                }
            }
        }
    }
}

func updateEntityToDatabase(object: ShelfObject) {
    // Current weight will not be updated in order to save real value.
    let ref = Database.database().reference()
    let objectID = "shelf\(String(describing: object.ID ?? 0))"
    ref.child("Shelves").child(objectID).child("id").setValue(object.ID) { (error, ref) in
        if error != nil {
            NSLog("Couldn't set ID value to Firebase - Error: \(String(describing: error?.localizedDescription))")
        }
    }
    ref.child("Shelves").child(objectID).child("type").setValue(object.type) { (error, ref) in
        if error != nil {
            NSLog("Couldn't set Type value to Firebase - Error: \(String(describing: error?.localizedDescription))")
        }
    }
    ref.child("Shelves").child(objectID).child("labelColor").setValue(object.labelColor) { (error, ref) in
        if error != nil {
            NSLog("Couldn't set Label Color value to Firebase - Error: \(String(describing: error?.localizedDescription))")
        }
    }
    ref.child("Shelves").child(objectID).child("limitWeight").setValue(object.limitWeight) { (error, ref) in
        if error != nil {
            NSLog("Couldn't set Limit Weight value to Firebase - Error: \(String(describing: error?.localizedDescription))")
        }
    }
}

func getTemperatureFromFirebaseAndSave(close: Bool) {
    let ref = Database.database().reference()
    let lastTimestamp = getLastTempTimestamp()
    if close {
        ref.removeAllObservers()
    } else {
        ref.child("Temperature").observe(.value) { (snapshot) in
            let snapshotDict = snapshot.value as? NSDictionary
            if snapshotDict != nil {
                for item in snapshotDict! {
                    let itemDict = item.value as? NSDictionary
                    if itemDict != nil {
                        let temp = itemDict?["temp"] as? Float ?? 0.0
                        let timeStamp = itemDict?["timestamp"] as? Float ?? 0.0
                        if timeStamp > lastTimestamp {
                        saveTemperature(temp: temp, timeStamp: timeStamp)
                        NSLog("Temperature successfuly got from Firebase - Temp: \(temp), TimeStamp: \(timeStamp)")
                        } else {
                        NSLog("That temperature is already saved - Temp: \(temp)")
                        }
                    }
                }
            }
        }
    }
}

func getHumidityFromFirebaseAndSave(close: Bool) {
    let ref = Database.database().reference()
    let lastTimestamp = getLastHumTimestamp()
    if close {
        ref.removeAllObservers()
    } else {
        ref.child("Humidity").observe(.value) { (snapshot) in
            let snapshotDict = snapshot.value as? NSDictionary
            if snapshotDict != nil {
                for item in snapshotDict! {
                    let itemDict = item.value as? NSDictionary
                    if itemDict != nil {
                        let hum = itemDict?["hum"] as? Float ?? 0.0
                        let timeStamp = itemDict?["timestamp"] as? Float ?? 0.0
                        if timeStamp > lastTimestamp {
                        saveHumidity(hum: hum, timeStamp: timeStamp)
                        NSLog("Humidity successfuly got from Firebase - Humidity: \(hum), TimeStamp: \(timeStamp)")
                        } else {
                            NSLog("That humidity is already saved - Humidity: \(hum)")
                        }
                    }
                }
            }
        }
    }
}

