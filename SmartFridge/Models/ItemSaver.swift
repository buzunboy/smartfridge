//
//  ItemSaver.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 19.11.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import UIKit

class ItemSaver: NSObject {
    
    var name: String = ""
    var ID: String = ""
    var amount: String = ""
    var color: String = ""
    
    init(ID: String, name: String, amount: String, color: String) {
        self.ID = ID
        self.name = name
        self.amount = amount
        self.color = color
    }

}
