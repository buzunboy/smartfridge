//
//  ShelfObject.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 4.12.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import UIKit

class ShelfObject: NSObject {

    var ID: Int?
    var type: String?
    var currentWeight: NSNumber?
    var limitWeight: NSNumber?
    var labelColor: Int?
    
    init(ID: Int?, type: String?, labelColor: Int?, limitWeight: NSNumber?, current: NSNumber?) {
        self.ID = ID
        self.type = type
        self.limitWeight = limitWeight
        self.labelColor = labelColor
        self.currentWeight = current
    }
}
