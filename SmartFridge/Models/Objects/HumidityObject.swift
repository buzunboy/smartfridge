//
//  HumidityObject.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 22.12.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import UIKit

class HumidityObject: NSObject {

    var hum: Float = 0.0
    var timeStamp: Float = 0.0

    init(hum: Float, timeStamp: Float) {
        self.hum = hum
        self.timeStamp = timeStamp
    }

}
