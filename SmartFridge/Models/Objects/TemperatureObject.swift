//
//  TemperatureObject.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 21.12.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import UIKit

class TemperatureObject: NSObject {
    
    var temp: Float = 0.0
    var timeStamp: Float = 0.0
    
    init(temp: Float, timeStamp: Float) {
        self.temp = temp
        self.timeStamp = timeStamp
    }

}
