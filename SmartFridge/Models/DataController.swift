//
//  DataController.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 4.12.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import UIKit
import CoreData

func getCorrespondingColorName(color: Int?) -> String? {
    var colorName = ""
    if color != nil {
        switch color! {
        case 0:
            colorName = "Red"
        case 1:
            colorName = "Green"
        case 2:
            colorName = "Blue"
        case 3:
            colorName = "Brown"
        case 4:
            colorName = "Magenta"
        case 5:
            colorName = "Cyan"
        case 6:
            colorName = "Purple"
        case 7:
            colorName = "Dark Gray"
        case 8:
            colorName = "Light Gray"
        case 9:
            colorName = "Black"
        default:
            colorName = "Red"
        }
    }
    return colorName
}

func getCorrespondingUIColor(color: Int?) -> UIColor {
    var colorUI = UIColor.red
    if color != nil {
        switch color! {
        case 0:
            colorUI = UIColor.red
        case 1:
            colorUI = UIColor.green
        case 2:
            colorUI = UIColor.blue
        case 3:
            colorUI = UIColor.brown
        case 4:
            colorUI = UIColor.magenta
        case 5:
            colorUI = UIColor.cyan
        case 6:
            colorUI = UIColor.purple
        case 7:
            colorUI = UIColor.darkGray
        case 8:
            colorUI = UIColor.lightGray
        case 9:
            colorUI = UIColor.black
        default:
            colorUI = UIColor.red
        }
    }
    return colorUI
}

func prepareCartItems(isMissing: Bool) -> [ShelfObject] {
    let allShelves = getShelfObjects()
    if isMissing {
        var shelfArray = [ShelfObject]()
        for item in allShelves {
            let limitW = Double(exactly: item.limitWeight ?? 0)
            let currW = Double(exactly: item.currentWeight ?? 0)
            if (currW?.isLess(than: limitW!))! {
                shelfArray.append(item)
            }
        }
        return shelfArray
    } else {
        return getShelfObjects()
    }
}


//class DataController: NSObject {
//    var persistentContainer = NSPersistentContainer()
//    var managedObjectContext: NSManagedObjectContext
//    init(completionClosure: @escaping () -> ()) {
//        persistentContainer = NSPersistentContainer(name: "CoreData")
//        persistentContainer.loadPersistentStores() { (description, error) in
//            if let error = error {
//                fatalError("Failed to load Core Data stack: \(error)")
//            }
//            completionClosure()
//        }
//    }
//
//}
/*
 init(completionClosure: @escaping () -> ()) {
 //This resource is the same name as your xcdatamodeld contained in your project
 guard let modelURL = Bundle.main.url(forResource: "DataModel", withExtension:"momd") else {
 fatalError("Error loading model from bundle")
 }
 // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
 guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
 fatalError("Error initializing mom from: \(modelURL)")
 }
 
 let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
 
 managedObjectContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
 managedObjectContext.persistentStoreCoordinator = psc
 
 let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
 queue.async {
 guard let docURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last else {
 fatalError("Unable to resolve document directory")
 }
 let storeURL = docURL.appendingPathComponent("DataModel.sqlite")
 do {
 try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
 //The callback block is expected to complete the User Interface and therefore should be presented back on the main queue so that the user interface does not need to be concerned with which queue this call is coming from.
 DispatchQueue.main.sync(execute: completionClosure)
 } catch {
 fatalError("Error migrating store: \(error)")
 }
 }
 }*/
