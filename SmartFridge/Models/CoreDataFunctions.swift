//
//  CoreDataFunctions.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 4.12.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import Foundation
import CoreData
import UIKit

// MARK: - Shelf Functions

func saveShelf(shelf: ShelfObject) {
    
    if shelf.type == nil {
        NSLog("Object couldn't saved - Reason: Type cannot be nil")
        return
    }
    
    if shelf.limitWeight == nil {
        NSLog("Object couldn't saved - Reason: Limit Weight cannot be nil")
        return
    }
    
    if shelf.labelColor == nil {
        let colors = getShelfColors()
        for color in 0...10 {
            if !colors.contains(color) {
                shelf.labelColor = color
                break
            }
        }
    }
    if shelf.labelColor == nil {
        NSLog("All colors are already set")
        return
    }
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Shelf")
    request.returnsObjectsAsFaults = false
    do {
        let results = try context.fetch(request)
        if results.count > 0 {
            var isMatched = false
            for result in results as! [NSManagedObject]{
                if let resultID = result.value(forKey: "id") as? Int {
                    if resultID == shelf.ID! {
                        result.setValue(shelf.currentWeight!, forKey: "currentWeight")
                        result.setValue(shelf.limitWeight!, forKey: "limitWeight")
                        result.setValue(shelf.labelColor!, forKey: "labelColor")
                        result.setValue(shelf.type, forKey: "type")
                        NSLog("Shelf Object, \(shelf.type!), is updated with properties - LabelColor: \(String(describing: result.value(forKey: "labelColor"))), LimitWeight: \(String(describing: result.value(forKey: "limitWeight"))), CurrentWeight: \(String(describing: result.value(forKey: "currentWeight")))")
                        isMatched = true
                    }
                }
            }
            if !isMatched {
                let saveObject = NSEntityDescription.insertNewObject(forEntityName: "Shelf", into: context)
                saveObject.setValue(shelf.currentWeight!, forKey: "currentWeight")
                saveObject.setValue(shelf.limitWeight!, forKey: "limitWeight")
                saveObject.setValue(shelf.labelColor!, forKey: "labelColor")
                saveObject.setValue(shelf.type, forKey: "type")
                saveObject.setValue(shelf.ID!, forKey: "id")
                try context.save()
                NSLog("Shelf Object, \(shelf.type!), is updated with properties - LabelColor: \(String(describing: shelf.labelColor)), LimitWeight: \(String(describing: shelf.limitWeight)), CurrentWeight: \(String(describing: shelf.currentWeight))")
            }
            try context.save()
            NSLog("Shelf Object, \(shelf.type!), is updated with properties - LabelColor: \(String(describing: shelf.labelColor)), LimitWeight: \(String(describing: shelf.limitWeight)), CurrentWeight: \(String(describing: shelf.currentWeight))")
            
        } else {
            let saveObject = NSEntityDescription.insertNewObject(forEntityName: "Shelf", into: context)
            saveObject.setValue(shelf.currentWeight!, forKey: "currentWeight")
            saveObject.setValue(shelf.limitWeight!, forKey: "limitWeight")
            saveObject.setValue(shelf.labelColor!, forKey: "labelColor")
            saveObject.setValue(shelf.type, forKey: "type")
            saveObject.setValue(shelf.ID!, forKey: "id")
            try context.save()
            NSLog("Shelf Object, \(shelf.type!), is updated with properties - LabelColor: \(String(describing: shelf.labelColor)), LimitWeight: \(String(describing: shelf.limitWeight)), CurrentWeight: \(String(describing: shelf.currentWeight))")
            
        }
    } catch {
        NSLog("Object couldn't saved - Reason: \(error.localizedDescription)")
        
    }
    //
    //    if !isAlreadySaved {
    //        let saveObject = NSEntityDescription.insertNewObject(forEntityName: "Shelf", into: context)
    //        saveObject.setValue(shelf.currentWeight!, forKey: "currentWeight")
    //        saveObject.setValue(shelf.limitWeight!, forKey: "limitWeight")
    //        saveObject.setValue(shelf.labelColor!, forKey: "labelColor")
    //        saveObject.setValue(shelf.type, forKey: "type")
    //        saveObject.setValue(shelf.ID!, forKey: "id")
    //
    //
    //        do {
    //            try context.save()
    //            NSLog("Shelf Object, \(shelf.type!), is saved with properties - LabelColor: \(String(describing: saveObject.value(forKey: "labelColor"))), LimitWeight: \(String(describing: saveObject.value(forKey: "limitWeight"))), CurrentWeight: \(String(describing: saveObject.value(forKey: "currentWeight")))")
    //        } catch  {
    //            NSLog("Object couldn't saved - Reason: \(error.localizedDescription)")
    //        }
    //    }
}

func getShelfColors() -> [Int] {
    var colors: [Int] = []
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Shelf")
    request.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(request)
        if results.count > 0 {
            for result in results as! [NSManagedObject]{
                if let color = result.value(forKey: "labelColor") as? Int {
                    colors.append(color)
                }
            }
        }
    } catch {
        
    }
    return colors
}

func getShelfObjects() -> [ShelfObject] {
    var shelfObjects = [ShelfObject]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Shelf")
    request.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(request)
        if results.count > 0 {
            for result in results as! [NSManagedObject]{
                var id = 0
                var type = ""
                var color = 0
                var curWeight: NSNumber?
                var limWeight: NSNumber?
                if let IDObj = result.value(forKey: "id") as? Int {
                    id = IDObj
                }
                if let colorObj = result.value(forKey: "labelColor") as? Int {
                    color = colorObj
                }
                if let typeObj = result.value(forKey: "type") as? String {
                    type = typeObj
                }
                if let curWeightObj = result.value(forKey: "currentWeight") as? NSNumber {
                    curWeight = curWeightObj
                }
                if let limWeightObj = result.value(forKey: "limitWeight") as? NSNumber {
                    limWeight = limWeightObj
                }
                NSLog("Shelf Object is loaded with properties - Type: \(type) & Color: \(color) & LimitWeight: \(String(describing: limWeight ?? 0)) & Current Weight: \(String(describing: curWeight ?? 0))")
                shelfObjects.append(ShelfObject(ID: id, type: type, labelColor: color, limitWeight: limWeight, current: curWeight))
            }
        }
    } catch {
        
    }
    shelfObjects = shelfObjects.sorted(by: { $0.ID! < $1.ID! })
    return shelfObjects
}

func getObject(withId: Int) -> ShelfObject? {
    var object : ShelfObject!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Shelf")
    request.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(request)
        if results.count > 0 {
            for result in results as! [NSManagedObject]{
                if let resultID = result.value(forKey: "id") as? Int {
                    if resultID == withId {
                        var type = ""
                        var color = 0
                        var curWeight: NSNumber?
                        var limWeight: NSNumber?
                        if let colorObj = result.value(forKey: "labelColor") as? Int {
                            color = colorObj
                        }
                        if let typeObj = result.value(forKey: "type") as? String {
                            type = typeObj
                        }
                        if let curWeightObj = result.value(forKey: "currentWeight") as? NSNumber {
                            curWeight = curWeightObj
                        }
                        if let limWeightObj = result.value(forKey: "limitWeight") as? NSNumber {
                            limWeight = limWeightObj
                        }
                        NSLog("Shelf Object is loaded with properties - Type: \(type) & Color: \(color) & LimitWeight: \(String(describing: limWeight ?? 0)) & Current Weight: \(String(describing: curWeight ?? 0))")
                        object = ShelfObject(ID: withId, type: type, labelColor: color, limitWeight: limWeight, current: curWeight)
                    }
                }
            }
        }
    } catch  {
        
    }
    return object
}

func getCurWeight(withId: Int) -> NSNumber {
    let obj = getObject(withId: withId)
    return (obj?.currentWeight) ?? 0
}

func removeAllEntities() {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Shelf")
    request.returnsObjectsAsFaults = true
    request.includesPropertyValues = false
    
    do {
        let results = try context.fetch(request)
        if results.count > 0 {
            for result in results as! [NSManagedObject]{
                let deleteReq = NSBatchDeleteRequest(fetchRequest: request)
                try context.execute(deleteReq)
                context.delete(result)
            }
        }
    } catch  {
        
    }
}

func removeShelf(shelf: ShelfObject) {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Shelf")
    request.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(request)
        if results.count > 0 {
            for result in results as! [NSManagedObject]{
                let resultType = result.value(forKey: "type") as? String ?? ""
                let resultColor = result.value(forKey: "labelColor") as? Int ?? 0
                if  resultType == shelf.type {
                    if resultColor == shelf.labelColor {
                        let deleteReq = NSBatchDeleteRequest(fetchRequest: request)
                        try context.execute(deleteReq)
                        context.delete(result)
                    }
                }
            }
        }
    } catch  {
        
    }
}

// MARK: - IP Functions

func saveIPAddress(of: String?, handler: @escaping (_ error: String?) -> Void) {
    if of != nil {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "IP")
        request.returnsObjectsAsFaults = false
        var isAlreadySaved = false
        do {
            let results = try context.fetch(request)
            if results.count > 0 {
                for result in results as! [NSManagedObject] {
                    if (result.value(forKey: "ipAddress") as? String) != nil {
                        result.setValue(of, forKey: "ipAddress")
                        NSLog("IP Address saved successfully")
                        isAlreadySaved = true
                        handler(nil)
                    }
                }
            }
        } catch {
            handler("Couldn't update the IP Address - Reason: \(error.localizedDescription)")
        }
        
        if !isAlreadySaved {
            let saveObject = NSEntityDescription.insertNewObject(forEntityName: "IP", into: context)
            saveObject.setValue(of!, forKey: "ipAddress")
        }
        
        do {
            try context.save()
            handler(nil)
        } catch {
            handler("Couldn't save the IP Address - Reason: \(error.localizedDescription)")
        }
        
    } else {
        handler("Ip address cannot be saved when it is empty")
    }
    
}

func getIPAddress(handler: @escaping (_ error: String?, _ result: String?) -> Void){
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "IP")
    request.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(request)
        if results.count > 0 {
            for result in results as! [NSManagedObject] {
                if let ipAddress = result.value(forKey: "ipAddress") as? String {
                    handler(nil, ipAddress)
                } else {
                    handler("Couldn't get the IP Address, results are empty", nil)
                }
            }
        }
    } catch {
        handler("Couldn't get the IP Address - Reason: \(error.localizedDescription)", nil)
    }
}

// MARK: - Temperature Functions

func saveTemperature(temp: Float, timeStamp: Float) {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    let isAlreadySaved = false
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Temperature")
    request.returnsObjectsAsFaults = false
    
//    do {
//        let results = try context.fetch(request)
//        if results.count > 0 {
//            for result in results as! [NSManagedObject] {
//                let timeStampDatabase = result.value(forKey: "timestamp") as? Float ?? 0
//                if timeStampDatabase == timeStamp && timeStampDatabase != 0 {
//                    isAlreadySaved = true
//                }
//            }
//        }
//    } catch {
//
//    }
    
    if !isAlreadySaved {
        let saveObject = NSEntityDescription.insertNewObject(forEntityName: "Temperature", into: context)
        saveObject.setValue(temp, forKey: "temp")
        saveObject.setValue(timeStamp, forKey: "timestamp")
        
        do {
            try context.save()
            NSLog("Temperature saved successfully - Temp:\(temp), Time: \(timeStamp)")
        } catch {
            NSLog("Temperature couldn't saved - Error: \(error.localizedDescription)")
        }
    }
}

func getTemperature(_numberOfItems: Int) -> [TemperatureObject] {
    var items = [TemperatureObject]()
    var obj = [TemperatureObject]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Temperature")
    request.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(request)
        if results.count > 0 {
            for result in results as! [NSManagedObject]{
                let timeStamp = result.value(forKey: "timestamp") as? Float ?? 0
                let temp = result.value(forKey: "temp") as? Float ?? 0
                obj.append(TemperatureObject(temp: temp, timeStamp: timeStamp))
            }
        }
    } catch {
    }
    
    items = obj.sorted(by: { $0.timeStamp > $1.timeStamp })
    if items.count > _numberOfItems {
        items.removeLast(items.count-_numberOfItems)
    }
    return items
}


func numberOfTemps() -> Int {
    var numberOfTemps = 0
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Temperature")
    request.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(request)
        numberOfTemps = (results.count > 50) ? 50 : results.count
    } catch {
        
    }
    return numberOfTemps
}

func saveLastTemperatureTime(){
    var latestTime: Float = 0
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Temperature")
    request.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(request)
        if results.count > 0 {
            for result in results as! [NSManagedObject]{
                let timestamp = result.value(forKey: "timestamp") as? Float ?? 0
                if timestamp > latestTime {
                    latestTime = timestamp
                }
            }
        }
    } catch {
        
    }
    
    let req = NSFetchRequest<NSFetchRequestResult>(entityName: "LastTemp")
    req.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(req)
        if results.count > 0 {
            for result in results as! [NSManagedObject] {
                result.setValue(latestTime, forKey: "timestamp")
            }
        } else {
            let saveObject = NSEntityDescription.insertNewObject(forEntityName: "LastTemp", into: context)
            saveObject.setValue(latestTime, forKey: "timestamp")
        }
        try context.save()
    } catch {
        
    }
}

func getLastTempTimestamp() -> Float {
    var latestTime: Float = 0
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "LastTemp")
    request.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(request)
        if results.count > 0 {
            for result in results as! [NSManagedObject] {
                latestTime = result.value(forKey: "timestamp") as? Float ?? 0
            }
        }
    } catch {
        
    }
    return latestTime
}

// MARK: - Humidity Functions

func saveHumidity(hum: Float, timeStamp: Float) {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    let isAlreadySaved = false
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Humidity")
    request.returnsObjectsAsFaults = false
    
//    do {
//        let results = try context.fetch(request)
//        if results.count > 0 {
//            for result in results as! [NSManagedObject] {
//                let timeStampDatabase = result.value(forKey: "timestamp") as? Float ?? 0
//                if timeStampDatabase == timeStamp && timeStampDatabase != 0 {
//                    isAlreadySaved = true
//                }
//            }
//        }
//    } catch {
//
//    }
    
    if !isAlreadySaved {
        let saveObject = NSEntityDescription.insertNewObject(forEntityName: "Humidity", into: context)
        saveObject.setValue(hum, forKey: "hum")
        saveObject.setValue(timeStamp, forKey: "timestamp")
        
        do {
            try context.save()
            NSLog("Humidity saved successfully - Humidity:\(hum), Time: \(timeStamp)")
        } catch {
            NSLog("Humidity couldn't saved - Error: \(error.localizedDescription)")
        }
    }

}

func getHumidity(_numberOfItems: Int) -> [HumidityObject] {
    var items = [HumidityObject]()
    var obj = [HumidityObject]()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Humidity")
    request.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(request)
        if results.count > 0 {
            for result in results as! [NSManagedObject]{
                let timeStamp = result.value(forKey: "timestamp") as? Float ?? 0
                let hum = result.value(forKey: "hum") as? Float ?? 0
                obj.append(HumidityObject(hum: hum, timeStamp: timeStamp))
            }
        }
    } catch {
    }
    
    items = obj.sorted(by: { $0.timeStamp > $1.timeStamp })
    if items.count > _numberOfItems {
       items.removeLast(items.count-_numberOfItems)
    }
    return items
}

func numberOfHums() -> Int {
    var numberOfHums = 0
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Humidity")
    request.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(request)
        numberOfHums = (results.count > 50) ? 50 : results.count
    } catch {
        
    }
    return numberOfHums
}

func saveLastHumidityTime(){
    var latestTime: Float = 0
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Humidity")
    request.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(request)
        if results.count > 0 {
            for result in results as! [NSManagedObject]{
                let timestamp = result.value(forKey: "timestamp") as? Float ?? 0
                if timestamp > latestTime {
                    latestTime = timestamp
                }
            }
        }
    } catch {
        
    }
    
    let req = NSFetchRequest<NSFetchRequestResult>(entityName: "LastHum")
    req.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(req)
        if results.count > 0 {
            for result in results as! [NSManagedObject] {
                result.setValue(latestTime, forKey: "timestamp")
            }
        } else {
            let saveObject = NSEntityDescription.insertNewObject(forEntityName: "LastHum", into: context)
            saveObject.setValue(latestTime, forKey: "timestamp")
        }
        try context.save()
    } catch {
        
    }
}

func getLastHumTimestamp() -> Float {
    var latestTime: Float = 0
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "LastHum")
    request.returnsObjectsAsFaults = false
    
    do {
        let results = try context.fetch(request)
        if results.count > 0 {
            for result in results as! [NSManagedObject] {
                latestTime = result.value(forKey: "timestamp") as? Float ?? 0
            }
        }
    } catch {
        
    }
    return latestTime
}
