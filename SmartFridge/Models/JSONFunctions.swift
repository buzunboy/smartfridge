//
//  JSONFunctions.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 13.11.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import Foundation
import UIKit

func getMainRequest(path: String, method: String, handler: @escaping (_ error: String?, _ loaded: [String:Any]?) -> Void) {
    var resultDict = [String:Any]()
    let json = [
        "Title": "Title",
        "Body": "Bodds"
    ]
    let jsonData = try? JSONSerialization.data(withJSONObject: json)
    
    // create post request
    var hostURL = "http://192.168.1.25:8000/"
    hostURL.append(path)
    let url = URL(string: hostURL)!
    var request = URLRequest(url: url)
    request.httpMethod = method.uppercased()
    
    // insert json data to the request
//    request.httpBody = jsonData
//    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//    request.setValue("application/json", forHTTPHeaderField: "Accept")

    
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
            //print(error?.localizedDescription ?? "No data")
            handler(error?.localizedDescription, nil)
            return
        }
        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
        print(responseJSON)
        if let responseJSON = responseJSON as? [String: Any] {
            NSLog("Response Result: \(responseJSON)")
            resultDict = responseJSON
            handler(nil, resultDict)
        }
    }
    
    task.resume()
}

func getAllItems(path: String, method: String, handler: @escaping (_ error: String?, _ loaded: NSArray?) -> Void) {
    var resultDict = NSArray()
    
    // create post request
    var hostURL = "http://192.168.1.22:8000/"
    hostURL.append(path)
    let url = URL(string: hostURL)!
    var request = URLRequest(url: url)
    request.httpMethod = method.uppercased()
    
    // insert json data to the request
    //    request.httpBody = jsonData
    //    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    //    request.setValue("application/json", forHTTPHeaderField: "Accept")
    
    
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
            //print(error?.localizedDescription ?? "No data")
            handler(error?.localizedDescription, nil)
            return
        }
        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
        if let responseJSON = responseJSON as? NSArray {
            NSLog("Response Result: \(responseJSON)")
            resultDict = responseJSON
            handler(nil, resultDict)
        }
    }
    
    task.resume()
}



func putRequest(data: [String:Any], path: String) -> [String:Any] {
    var resultDict = [String:Any]()
    let json = data
    
    let jsonData = try? JSONSerialization.data(withJSONObject: json)
    
    // create post request
    var hostURL = "localhost:8000/"
    hostURL.append(path)
    let url = URL(string: hostURL)!
    var request = URLRequest(url: url)
    request.httpMethod = "PUT"
    
    // insert json data to the request
    request.httpBody = jsonData
    
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
            print(error?.localizedDescription ?? "No data")
            return
        }
        let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
        if let responseJSON = responseJSON as? [String: Any] {
            NSLog("Response Result: \(responseJSON)")
            resultDict = responseJSON
        }
    }
    
    task.resume()
    
    return resultDict
}

func isConnectionSuccessful(handler: @escaping (_ connected: Bool?) -> Void) {
    let ip = ipCache.value(forKey: "fridgeIP") as? String ?? ""
    if ip != "" {
        let hostURL = "\(ip):8000/connection"
        let url = URL(string: hostURL)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                handler(false)
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if (responseJSON as? [String: Any]) != nil {
                handler(true)
            }
        }
        
        task.resume()
    } else {
        handler(false)
    }

}
