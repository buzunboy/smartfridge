//
//  LoginViewController.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 29.11.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import UIKit
import CoreData

class LoginViewController: UIViewController {

    @IBOutlet weak var ipTF: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    var dotTB = UIToolbar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getIPAddress { (error, result) in
            if error != nil {
                NSLog(error!)
            } else {
                if result != nil {
                    self.ipTF.text = result!
                }
            }
        }
//        if ipCache.value(forKey: "fridgeIP") != nil {
//            ipTF.text = ipCache.value(forKey: "fridgeIP") as? String ?? ""
//        }
        createDotToolbar()
        ipTF.inputAccessoryView = dotTB
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getShelvesFromFirebase()
        getTemperatureFromFirebaseAndSave(close: false)
        getHumidityFromFirebaseAndSave(close: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginBtnClicked(_ sender: Any) {
        if ipTF.text != "" {
            //ipCache.set(ipTF.text, forKey: "fridgeIP")
            saveIPAddress(of: ipTF.text, handler: { (error) in
                if error != nil {
                    NSLog(error!)
                }
            })
            isConnectionSuccessful(handler: { (connected) in
                if connected! {
                    let tabVC = mainStoryboard.instantiateViewController(withIdentifier: "tabBarVC")
                    self.present(tabVC, animated: true, completion: nil)
                }
            })
        }
        let tabVC = mainStoryboard.instantiateViewController(withIdentifier: "tabBarVC")
        self.present(tabVC, animated: true, completion: nil)
    }
    
    func createDotToolbar() {
        let dotBtn = UIBarButtonItem(title: ".", style: .done, target: self, action: #selector(addDot))
        dotTB.items = [dotBtn]
        dotTB.barStyle = UIBarStyle.default
        dotTB.isTranslucent = true
        dotTB.sizeToFit()
    }
    
    @objc func addDot() {
        ipTF.text = ipTF.text! + "."
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
