//
//  ShelvesTableViewController.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 29.11.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import UIKit
import CoreData

class shelveTableViewCell: UITableViewCell {
    @IBOutlet weak var shelveTitle: UILabel!
    @IBOutlet weak var productType: UILabel!
    @IBOutlet weak var currentWeightLabel: UILabel!
    @IBOutlet weak var warnLabel: UILabel!
    @IBOutlet weak var labelColorView: UIView!
    @IBOutlet weak var labelColorLabel: UILabel!
}

var selectedID = 0

class ShelvesTableViewController: UITableViewController {
    
    var tableObjects: [ShelfObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.largeTitleDisplayMode = .never
        self.navigationItem.title = "Shelves"
        prepareTable()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareTable() {
        tableObjects.removeAll()
        tableObjects = getShelfObjects()
        tableView.reloadData()
    }

    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "shelveTableViewCell", for: indexPath) as! shelveTableViewCell
        
        // Configure the cell...
        cell.shelveTitle.text = "Shelf \(String(describing: indexPath.row + 1))"
        if (tableObjects.count >= indexPath.row + 1){
            cell.productType.text = tableObjects[indexPath.row].type
            cell.labelColorLabel.text = getCorrespondingColorName(color: tableObjects[indexPath.row].labelColor)
            cell.labelColorView.backgroundColor = getCorrespondingUIColor(color: tableObjects[indexPath.row].labelColor)
            cell.currentWeightLabel.text = "Current: \(String(describing: tableObjects[indexPath.row].currentWeight ?? 0))"
            cell.warnLabel.text = "Warn me when \(String(describing: tableObjects[indexPath.row].limitWeight ?? 0)) kg"
            
            cell.labelColorView.layer.shadowColor = UIColor.black.cgColor
            cell.labelColorView.layer.shadowOpacity = 0.5
            cell.labelColorView.layer.shadowOffset = CGSize(width: 0, height: 10)
            cell.labelColorView.layer.shadowRadius = 5
            cell.labelColorView.layer.cornerRadius = cell.labelColorView.frame.size.width/2
            cell.labelColorLabel.layer.shadowColor = UIColor.black.cgColor
            cell.labelColorLabel.layer.shadowOpacity = 0.5
            cell.labelColorLabel.layer.shadowOffset = CGSize(width: 0, height: 10)
            cell.labelColorLabel.layer.shadowRadius = 5
            cell.labelColorLabel.layer.cornerRadius = cell.labelColorView.frame.size.width/2
        } else {
            cell.productType.text = "Product Type"
            cell.labelColorView.backgroundColor = UIColor.red
            cell.labelColorLabel.text = "Label Color"
            cell.currentWeightLabel.text = "Current Weight"
            cell.warnLabel.text = "Warn me when..."
            
            cell.labelColorView.layer.shadowColor = UIColor.black.cgColor
            cell.labelColorView.layer.shadowOpacity = 0.5
            cell.labelColorView.layer.shadowOffset = CGSize(width: 0, height: 10)
            cell.labelColorView.layer.shadowRadius = 5
            cell.labelColorView.layer.cornerRadius = cell.labelColorView.frame.size.width/2
            cell.labelColorLabel.layer.shadowColor = UIColor.black.cgColor
            cell.labelColorLabel.layer.shadowOpacity = 0.5
            cell.labelColorLabel.layer.shadowOffset = CGSize(width: 0, height: 10)
            cell.labelColorLabel.layer.shadowRadius = 5
            cell.labelColorLabel.layer.cornerRadius = cell.labelColorView.frame.size.width/2
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedID = indexPath.row
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let editVC = storyboard.instantiateViewController(withIdentifier: "EditShelfViewController") as! EditShelfViewController
        self.navigationController?.pushViewController(editVC, animated: true)
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
