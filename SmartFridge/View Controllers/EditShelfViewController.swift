//
//  EditShelfViewController.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 10.12.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import UIKit
import CoreData

class EditShelfViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var typeField: UITextField!
    @IBOutlet weak var limitWeightField: UITextField!
    @IBOutlet weak var labelColorField: UITextField!
    @IBOutlet weak var labelColorLabel: UILabel!
    @IBOutlet weak var labelColorView: UIView!
    @IBOutlet weak var shelfLabel: UILabel!
    @IBOutlet weak var curWeightLabel: UILabel!
    
    var colorPicker = UIPickerView()
    var pickerArray = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    var selectedColor: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colorPicker.dataSource = self
        colorPicker.delegate = self
        labelColorField.inputView = colorPicker
        prepareShadow()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        prepareFields()
        preparePickerAndToolbars()
        let doneItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneItemClicked))
        self.navigationItem.rightBarButtonItem = doneItem
        NSLog("Shelf \(selectedID+1) is selected")
        shelfLabel.text = "Shelf \(selectedID+1)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Preparations
    
    func prepareShadow() {
        labelColorView.layer.shadowColor = UIColor.black.cgColor
        labelColorView.layer.shadowOpacity = 0.5
        labelColorView.layer.shadowOffset = CGSize(width: 0, height: 10)
        labelColorView.layer.shadowRadius = 5
        
        labelColorField.layer.shadowColor = UIColor.black.cgColor
        labelColorField.layer.shadowOpacity = 0.5
        labelColorField.layer.shadowOffset = CGSize(width: 0, height: 10)
        labelColorField.layer.shadowRadius = 5
        
        labelColorLabel.layer.shadowColor = UIColor.black.cgColor
        labelColorLabel.layer.shadowOpacity = 0.5
        labelColorLabel.layer.shadowOffset = CGSize(width: 0, height: 10)
        labelColorLabel.layer.shadowRadius = 5
        
        labelColorView.layer.cornerRadius = labelColorView.frame.size.width/2
    }
    
    func prepareFields() {
        typeField.placeholder = "Enter the name"
        limitWeightField.placeholder = "Enter the limit for warning"
        labelColorField.placeholder = "Select color"
        let object = getObject(withId: selectedID)
        curWeightLabel.text = String(describing: object?.currentWeight ?? 0)
        if curWeightLabel.text == "nil" {
            curWeightLabel.text = "0"
        }
        typeField.text = object?.type
        limitWeightField.text = String(describing: object?.limitWeight ?? 0)
        labelColorField.text = getCorrespondingColorName(color: object?.labelColor)
        selectedColor = object?.labelColor
        labelColorView.backgroundColor = getCorrespondingUIColor(color: object?.labelColor)
    }

    
    @objc func doneItemClicked() {
        if typeField.text == "" {
            showAlertWithOneButton("Couldn't Edit", message: "Type cannot be empty", withAction: "OK")
            return
        }
        if limitWeightField.text == "" {
            showAlertWithOneButton("Couldn't Edit", message: "Limit Weight cannot be empty", withAction: "OK")
            return
        }
        if labelColorField.text == "" {
            showAlertWithOneButton("Couldn't Edit", message: "Label Color cannot be empty", withAction: "OK")
            return
        }
        
        let type = typeField.text
        let limWeight = Double(limitWeightField.text ?? "0")
        let labelColor = selectedColor
        let object = ShelfObject(ID: selectedID, type: type, labelColor: labelColor, limitWeight: NSNumber(value: limWeight ?? 0), current: getCurWeight(withId: selectedID))
        saveShelf(shelf: object)
        updateEntityToDatabase(object: object)
        
    }
    
    // MARK: - Picker View
    
    func preparePickerAndToolbars() {
        let colorArray = getShelfColors()
        for color in colorArray {
            if pickerArray.contains(color) {
                let pos = pickerArray.index(of: color)
                pickerArray.remove(at: pos!)
            }
        }
        colorPicker.reloadAllComponents()
        
        let accToolbar = UIToolbar()
        accToolbar.barStyle = UIBarStyle.default
        accToolbar.isTranslucent = true
        accToolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClicked))
        accToolbar.setItems([doneButton], animated: true)
        
        typeField.inputAccessoryView = accToolbar
        limitWeightField.inputAccessoryView = accToolbar
        labelColorField.inputAccessoryView = accToolbar
    }
    
    @objc func doneClicked() {
        view.endEditing(true)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return getCorrespondingColorName(color: pickerArray[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        labelColorField.text = getCorrespondingColorName(color: pickerArray[row])
        labelColorView.backgroundColor = getCorrespondingUIColor(color: pickerArray[row])
        selectedColor = pickerArray[row]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
