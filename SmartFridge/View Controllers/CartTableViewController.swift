//
//  CartTableViewController.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 13.11.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import UIKit

class cartTableViewCell: UITableViewCell {
    @IBOutlet weak var colorFrame: UIView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDetail: UILabel!
}

class CartTableViewController: UITableViewController {
    
    var dict = [String]()
    var items = [ShelfObject]()
    @IBOutlet weak var segmentedView: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Checkout", style: .done, target: self, action: #selector(goCheckout))
        if segmentedView.selectedSegmentIndex == 0 {
            getItems(isMissing: true)
        } else {
            getItems(isMissing: false)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartTableViewCell", for: indexPath) as! cartTableViewCell
        
        // Configure the cell...
        cell.itemName.text = items[indexPath.row].type
        cell.itemDetail.text = String(describing: items[indexPath.row].currentWeight ?? 0)
        cell.colorFrame.backgroundColor = getCorrespondingUIColor(color: items[indexPath.row].labelColor)
        cell.colorFrame.layer.cornerRadius = cell.colorFrame.frame.size.width/2
        return cell
    }
    
    func getItems(isMissing: Bool) {
        items.removeAll()
        tableView.reloadData()
        items = prepareCartItems(isMissing: isMissing)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
//        getAllItems(path: "weight", method: "get") { (error, result) in
//            if error != nil {
//                NSLog("Couldn't get the request - Error: \(error!)")
//            } else {
//                if result != nil {
//                    for item in result! {
//                        let itemDict = item as! NSDictionary
//                        let ID = itemDict["_id"] as? String ?? ""
//                        let name = itemDict["title"] as? String ?? ""
//                        let text = itemDict["text"] as? String ?? ""
//                        self.items.append(ItemSaver(ID: ID, name: name, amount: text, color: text))
//                    }
//                    DispatchQueue.main.async {
//                        self.tableView.reloadData()
//                    }
//                }
//            }
//        }
    }
    
    // MARK: - Segment View Function
    @IBAction func segmentChanged(_ sender: Any) {
        if segmentedView.selectedSegmentIndex == 0 {
            getItems(isMissing: true)
        } else {
            getItems(isMissing: false)
        }
    }
    
    @objc func goCheckout() {
        let wVC = mainStoryboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
        wVC.typeName = "cola"
        self.navigationController?.pushViewController(wVC, animated: true)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
