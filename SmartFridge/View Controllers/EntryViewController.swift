//
//  EntryViewController.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 22.12.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import UIKit

class EntryViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var prepareLabel: UILabel!
    
    var stringArray = ["Preparing your fridge", "Getting ready your fridge", "Your fridge is waiting for you", "Let's see what we have", "Hello World"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.startAnimating()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        changeLabel()
        getShelvesFromFirebase()
        getTemperatureFromFirebaseAndSave(close: false)
        getHumidityFromFirebaseAndSave(close: false)
        saveLastTemperatureTime()
        saveLastHumidityTime()
        Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { (time) in
            let tabVC = mainStoryboard.instantiateViewController(withIdentifier: "tabBarVC")
            self.present(tabVC, animated: true, completion: nil)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func changeLabel() {
        let randomIndex = Int(arc4random_uniform(UInt32(self.stringArray.count)))
        self.prepareLabel.text = self.stringArray[randomIndex]
        let anm = CATransition.init()
        anm.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        anm.type = kCATransitionPush
        anm.duration = 1
        prepareLabel.layer.add(anm, forKey: kCATransitionFromTop)
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (time) in
            let randomIndex = Int(arc4random_uniform(UInt32(self.stringArray.count)))
            self.prepareLabel.layer.add(anm, forKey: kCATransitionFromTop)
            self.prepareLabel.text = self.stringArray[randomIndex]
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
