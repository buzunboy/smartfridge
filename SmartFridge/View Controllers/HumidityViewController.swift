//
//  HumidityViewController.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 22.12.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import UIKit
import Charts

class HumidityViewController: UIViewController {

    @IBOutlet weak var lineChart: LineChartView!
    @IBOutlet weak var mSlider: UISlider!
    @IBOutlet weak var measurementLabel: UILabel!
    var data: [Float] = [0, 2, 4, 6]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.largeTitleDisplayMode = .never
        self.navigationItem.title = "Fridge Humidity"
        prepareSlider()
        reloadData(totalItem: 8)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        getHumidityFromFirebaseAndSave(close: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setChart(points: [Float]) {
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<points.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: Double(points[i]))
            dataEntries.append(dataEntry)
        }
        lineChart.dragEnabled = false
        lineChart.drawGridBackgroundEnabled = false
        lineChart.chartDescription?.text = ""
        lineChart.legend.enabled = false
        let chartDataSet = LineChartDataSet(values: dataEntries, label: "Humidity")
        chartDataSet.lineDashLengths = [5, 2.5]
        chartDataSet.highlightLineDashLengths = [5, 2.5]
        chartDataSet.setColor(.black)
        chartDataSet.setCircleColor(.black)
        chartDataSet.lineWidth = 1
        chartDataSet.circleRadius = 3
        chartDataSet.drawCircleHoleEnabled = false
        chartDataSet.valueFont = .systemFont(ofSize: 9)
        chartDataSet.formLineDashLengths = [5, 2.5]
        chartDataSet.formLineWidth = 1
        chartDataSet.formLineWidth = 15
        chartDataSet.mode = .horizontalBezier
        chartDataSet.drawHorizontalHighlightIndicatorEnabled = false
        
        let gradientColors = [ChartColorTemplates.colorFromString("#0000ff").cgColor,
                              ChartColorTemplates.colorFromString("#0000ff").cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        chartDataSet.fillAlpha = 1
        chartDataSet.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        chartDataSet.drawFilledEnabled = true
        
        
        let lineData = LineChartData(dataSet: chartDataSet)
        lineChart.data = lineData
        
        lineChart.drawGridBackgroundEnabled = false
        lineChart.drawMarkers = false
    }
    
    @objc func reloadData(totalItem: Int) {
        DispatchQueue.main.async {
            let obj = getHumidity(_numberOfItems: totalItem)
            self.data.removeAll()
            for item in obj {
                self.data.append(item.hum)
            }
            self.setChart(points: self.data)
        }
    }
    
    // MARK: - Slider Functions
    
    func prepareSlider() {
        mSlider.minimumValue = 4
        let num = Float(numberOfHums())
        mSlider.maximumValue = num
        mSlider.value = (num < 8) ? num : 8
    }
    
    @IBAction func changedSliderValue(_ sender: Any) {
        measurementLabel.text = "Show last \(String(describing: Int(mSlider.value))) measurements"
        reloadData(totalItem: Int(mSlider.value))
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
