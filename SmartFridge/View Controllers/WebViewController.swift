//
//  WebViewController.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 17.12.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
    
    @IBOutlet var webView: WKWebView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var webToolbar: UIToolbar!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var fwButton: UIBarButtonItem!
    var typeName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.uiDelegate = self
        webView.navigationDelegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.largeTitleDisplayMode = .never
        self.navigationItem.title = "Loading..."
        webView.load(URLRequest(url: URL(string: "http://www.hepsiburada.com/ara?q=\(typeName)")!))
        progressView.progress = 0.0
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.navigationItem.title = webView.title
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progressView.setProgress(Float(webView.estimatedProgress), animated: true)
            if webView.estimatedProgress >= 1.0 {
                progressView.alpha = 0.0
            } else {
                progressView.alpha = 1.0
            }
            
            if webView.canGoForward {
                fwButton.isEnabled = true
            } else {
                fwButton.isEnabled = false
            }
            
            if webView.canGoBack {
                backButton.isEnabled = true
            } else {
                backButton.isEnabled = false
            }
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        webView.goBack()
    }
    
    @IBAction func goForward(_ sender: Any) {
        webView.goForward()
        
    }
    
    @IBAction func openSafari(_ sender: Any) {
        UIApplication.shared.open(URL.init(string: "http://www.hepsiburada.com/ara?q=\(typeName)")!, options: [:], completionHandler: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
