//
//  AboutTableViewController.swift
//  SmartFridge
//
//  Created by Burak Uzunboy on 29.11.2017.
//  Copyright © 2017 Burak Uzunboy. All rights reserved.
//

import UIKit
import Charts
import LocalAuthentication

class SettingsTableViewController: UITableViewController {
    
    var error: NSError?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
        case 0:
            return 1
        case 1:
            return 3
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0 {
            let detailVC = mainStoryboard.instantiateViewController(withIdentifier: "DetailAboutViewController") as! DetailAboutViewController
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
        if indexPath.section == 1 && indexPath.row == 0 {
            // Touch ID Protection
            useTouchID(handler: { (error) in
                if error != nil {
                    DispatchQueue.main.async() { () -> Void in
                    showAlertWithOneButton("Couldn't Enter", message: error!, withAction: "OK")
                    }
                } else {
                    let shelveVC = mainStoryboard.instantiateViewController(withIdentifier: "ShelvesTableViewController") as! ShelvesTableViewController
                    DispatchQueue.main.async() { () -> Void in
                        self.navigationController?.pushViewController(shelveVC, animated: true)
                    }
                }
            })
        }
        if indexPath.section == 1 && indexPath.row == 1 {
            // Touch ID Protection
            useTouchID(handler: { (error) in
                if error != nil {
                    DispatchQueue.main.async() { () -> Void in
                    showAlertWithOneButton("Couldn't Enter", message: error!, withAction: "OK")
                    }
                } else {
                    let tempVC = mainStoryboard.instantiateViewController(withIdentifier: "TempViewController") as! TempViewController
                    DispatchQueue.main.async() { () -> Void in
                        self.navigationController?.pushViewController(tempVC, animated: true)
                    }
                }
            })
        }
        if indexPath.section == 1 && indexPath.row == 2 {
            // Touch ID Protection
            useTouchID(handler: { (error) in
                if error != nil {
                    DispatchQueue.main.async() { () -> Void in
                        showAlertWithOneButton("Couldn't Enter", message: error!, withAction: "OK")
                    }
                } else {
                    let humVC = mainStoryboard.instantiateViewController(withIdentifier: "HumidityViewController") as! HumidityViewController
                    DispatchQueue.main.async() { () -> Void in
                        self.navigationController?.pushViewController(humVC, animated: true)
                    }
                }
            })
        }
        if indexPath.section == 2 && indexPath.row == 0 {
            let loginVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            let rootVC = UIApplication.shared.delegate?.window!
            UIView.transition(with: rootVC!, duration: 1, options: .transitionFlipFromLeft, animations: {
                rootVC?.rootViewController = loginVC
            }, completion: nil)
            
        }
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 {
            return true // Change false if doesn't want to highlight the cell
        } else {
            return true
        }
    }
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
